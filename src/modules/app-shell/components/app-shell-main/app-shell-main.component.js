'use strict';

import template from './app-shell-main.tpl.html';
import controller from './app-shell-main.controller';
import './app-shell-main.scss';

export const AppShellMainComponent = {
    template,
    controller
};