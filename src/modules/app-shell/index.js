'use strict';

import { AppShellMainComponent } from './components/app-shell-main/app-shell-main.component';

export default angular.module('app-shell', [])
    .component('appShellMain', AppShellMainComponent);
