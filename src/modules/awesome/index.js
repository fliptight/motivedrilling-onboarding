'use strict';

import ngRedux from 'ng-redux';
import { FoobarComponent } from './components/foobar/foobar.component';

export default angular.module('awesome', [ngRedux])
    .component('foobar', FoobarComponent);