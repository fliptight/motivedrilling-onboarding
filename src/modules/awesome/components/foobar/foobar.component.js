'use strict';

import template from './foobar.tpl.html';
import controller from './foobar.controller';

export const FoobarComponent = {
    template,
    controller
};