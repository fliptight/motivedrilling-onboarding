'use strict';

export default class FoobarController {
    constructor ($timeout) {
        this.$timeout = $timeout;
    }

    doSomething() {
        this.$timeout(() => alert('did something after 1 second'), 1000);
    }
}

FoobarController.$inject = ['$timeout'];