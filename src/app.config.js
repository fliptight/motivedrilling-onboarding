'use strict';

export default function AppConfig($stateProvider) {
    $stateProvider.state({
        name: 'root',
        url: '',
        views: {
            app: 'appShellMain',
        },
    });

    $stateProvider.state({
        name: 'root.foobar',
        url: '/foobar',
        views: {
            body: 'foobar',
        },
    });
}

AppConfig.$inject = ['$stateProvider'];