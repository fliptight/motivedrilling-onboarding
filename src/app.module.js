'use strict';

import 'angular';
import angular from 'angular';
import uirouter from '@uirouter/angularjs';
import AppShell from './modules/app-shell';
import Awesome from './modules/awesome';
import AppConfig from './app.config';

export const AppModule = angular.module('app', [uirouter, AppShell.name, Awesome.name])
    .config(AppConfig);