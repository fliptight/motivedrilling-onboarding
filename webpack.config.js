const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const devPort = 3000;

var config = {
    mode: 'development',
    target: 'web',
    context: path.resolve(__dirname, './src'),
    entry: {
        'motive': './index.js'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: '[name].bundle.js',
        publicPath: 'http://localhost:' + devPort + '/',
        chunkFilename: '[name].bundle.js',
    },
    optimization: {
        minimize: false
    },
    module: getModules(),
    resolve: getResolve(),
    plugins: getPlugins(),
    devServer: getDevServer()
};

function getModules() {
    const dist_dirs = [
        path.resolve(__dirname, 'node_modules'),
        path.resolve(__dirname, 'dist'),
    ];

    return {
        rules: [
            {
                test: /\.js$/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['es2015', 'stage-3'],
                            plugins: [
                                [
                                    'module-resolver', 
                                    {
                                        'root': ['./src']
                                    }
                                ]
                            ]
                        }
                    }
                ],
                exclude: [...dist_dirs],
                include: [
                    path.resolve(__dirname, './src'),
                ],
            },
            {
                test: /\.tpl\.html$/,
                loader: 'html-loader',
            },
            {
                test: /\.(sa|sc)ss$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader', options: { importLoaders: 1, sourceMap: true } },
                    { loader: 'postcss-loader', options: { sourceMap: true } },
                    { loader: 'sass-loader', options: {
                        sourceMap: true,
                        include: [
                            path.resolve(__dirname, 'src/assets/scss')
                        ],
                    },
                    },
                ],
            },

        ]
    };
}

function getResolve() {
    return {
        alias: {
            assets: path.resolve(__dirname, 'assets/'),
            svScss: path.resolve(__dirname, 'assets/scss/'),
        },
        modules: [
            path.resolve(__dirname, './src'),
            'node_modules',
        ]
    }
}

function getPlugins() {
    return [
        new HtmlWebpackPlugin({
            title: 'Motive Dashboard',
            inject: 'body',
            template: './app.html',
            filename: 'index.html',
        })
    ];
}

function getDevServer() {
    return {
        contentBase: [
            path.join(__dirname, 'dist'),
            path.join(__dirname, 'src/assets'),
            path.join(__dirname, 'assets'),
        ],
        publicPath: 'http://localhost:' + devPort + '/',
        host: '0.0.0.0',
        port: devPort,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        compress: true,
        historyApiFallback: true,
        inline: true,
    };
}

module.exports = config;