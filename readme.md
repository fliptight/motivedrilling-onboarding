This project demonstrates the minimum amount of tech required to put together the basic architecture of the Dashboard app.


- NodeJS
    - npm scripts
    - nps
- Module formats
    - Common JS
    - ES6 Module
    - Webpack
- Javascript
    - ES5, ES6, ES7
- Angular 1.6.x
    - Components
    - UI Router
    - UI Bootstrap
    - ngRedux
- SCSS


To clone
---------

`git clone https://bitbucket.org/fliptight/motivedrilling-onboarding.git`


To install everything and run app
---------------------------------

`npm start`

Visit demo URLs
---------------

http://localhost:3000 - loads app shell view 
 
http://localhost:3000/#!/foobar - loads view with foobar component


Additional Commands
-------------------

`npm run build`

`npm run dev`