'use strict';

const path = require('path');

module.exports = {
    plugins: {
        'postcss-import': {
            assets: path.resolve(__dirname, './src/assets/scss')
        }
    }
};